FROM pytorch/pytorch:1.4-cuda10.1-cudnn7-runtime

## ensure locale is set during build
ENV LANG C.UTF-8

ARG DEBIAN_FRONTEND=noninteractive

RUN pip install --no-cache jupyter && \
    pip install --no-cache jupyterhub==1.0.0 && \
    pip install --no-cache jupyterlab==1.1.4 && \
    pip install --no-cache notebook==6.0.1 && \
    echo ok

RUN apt-get update && \
    apt-get install -y git debconf-utils && \
    echo "krb5-config krb5-config/add_servers_realm string CERN.CH" | debconf-set-selections && \
    echo "krb5-config krb5-config/default_realm string CERN.CH" | debconf-set-selections && \
    apt-get install -y krb5-user && \
    apt-get install -y nvidia-cuda-toolkit && \
    echo ok

ARG NB_USER=jovyan
ARG NB_UID=1000
ENV USER ${NB_USER}
ENV HOME /home/${NB_USER}
ENV SHELL bash

RUN adduser --uid ${NB_UID} ${NB_USER} && passwd -d ${NB_USER}

WORKDIR ${HOME}
USER ${USER}
